import datetime
import os
from file_operations import FileOperations

file_ops = FileOperations()

class Logging:

    def __init__(self): 
        self.directory = os.path.expanduser("~/.pasty")
        self.bak_dir   = os.path.expanduser("~/.bak_pasty")

    def _write_log_entry(self, log_entry, log_type):
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        log_type  = f"{log_type}"
        files     = [self.directory, self.bak_dir]
        for file in files:
            log_files    = []
            log_filename = os.path.join(file, log_type)
            log_files.append(log_filename)
            with open(log_filename, "a") as log_file:
                log_file.write(f"[{timestamp}] {log_entry}\n")
            for logs in log_files:
                file_ops.set_file_permissions(logs)

    def log_message(self, message=None, **kwargs):
        log_type   = kwargs.get("log_type")
        filename  = kwargs.get("filename")
        key_type  = kwargs.get("key_type")
        if not message:
            if log_type and filename:
                message = f"file written successfully to {log_type}/{filename}"
            elif key_type:
                message = f"generated a new encryption key of type: {key_type}"
        self._write_log_entry(message, log_type)
