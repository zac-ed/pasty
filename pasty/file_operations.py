import os
import sys
import subprocess
from pasty_logging import Logging

logging = Logging()

class FileOperations:
    def get_argument(self, number):
       '''
       Takes first command line argument after
       the program name and returns it for 
       later use
       '''
       argument = sys.argv[number]
       return argument 

    def get_target_file(self, directory, sub_dir, name):
        '''
        returns the full path of a target file
        '''
        try:
            target_dir  = os.path.join(directory, sub_dir)
            target_file = os.path.join(target_dir, name)
            target_file = os.path.expanduser(target_file)
            if os.path.exists(target_file):
                return target_file 
        except Exception:
            log_filename = f"{sub_dir}.txt"
            logging.log_message(log_type=log_filename, message="file not found") 

    def write_to_file(self, target_file, content, filename, log_type):
        '''
        writes content to a file, writes as binary

        Args:
            target_file (full file path), content (the string to be written),
            filename, log_type (the filename for the log)
        '''
        with open(target_file, "wb") as file:
            file.write(content)
            if filename == "salt":
                logging.log_message(log_type=log_type, filename="master")
            else: 
                logging.log_message(log_type=log_type, filename=filename)

    def retrieve_file_content(self, target_file, log_type):
        '''
        Returns the content of a file for 
        a service specified by the user
        Make sure to enter the full_path via full_path
        '''
        try:
            with open(target_file, "r") as file:
                content = file.read()
            return content
        except Exception as e:
            error = f"{target_file} not found: {e}"
            logging.log_message(message=error, log_type=log_type)
     
    def set_file_permissions(self, file_path):
        subprocess.run(["chmod", "600", file_path])

    def make_copies(self, input_file, output_file):
       subprocess.run(["cp", input_file, output_file])
       self.set_file_permissions(output_file)
     
    def full_path(self, directory, sub_dir, file_name):
       return os.path.expanduser(os.path.join(directory, sub_dir, file_name))

#    def get_missing_files(self, file_list):
#        missing_files = [file for file in file_list if not os.path.exists(file)]
#        return missing_files
#
#    def get_existing_files(self, file_list):
#        existing_files = [file for file in file_list if os.path.exists(file)]
#        return existing_files
