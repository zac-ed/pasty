import os
from file_operations import FileOperations

file_ops = FileOperations()

class Checks():

    def check_sudo(self):
        return os.geteuid() == 0

    def check_first_time(self):
        if file_ops.get_target_file("~/.pasty", "logs", "initialised") and file_ops.get_target_file("~/.pasty", "logs", "initialised"):
            return None 
        elif file_ops.get_target_file("~/.pasty", "logs", "initialised") and not file_ops.get_target_file("~/.pasty", "logs", "initialised"):
            file_ops.make_copies(file_ops.full_path("~/.pasty", "logs", "initialised"), file_ops.full_path("~/.pasty", "logs", "initialised"))
            return None 
        elif file_ops.get_target_file("~/.bak_pasty", "logs", "initialised") and not file_ops.get_target_file("~/.pasty", "logs", "initialised"):
            file_ops.make_copies(file_ops.full_path("~/.pasty", "logs", "initialised"), file_ops.full_path("~/.pasty", "logs", "initialised"))
            return None 
        else:
            return True

    def check_master_exists(self):
        all_files_exist = (
            file_ops.get_target_file("~/.pasty", "master", "master") and
            file_ops.get_target_file("~/.pasty", "salt", "salt") and
            file_ops.get_target_file("~/.bak_pasty", "master", "master") and
            file_ops.get_target_file("~/.bak_pasty", "salt", "salt")
        )
        missing_master = False
        missing_salt = False
        if all_files_exist:
            return True
        else:
            if (
                not file_ops.get_target_file("~/.pasty", "master", "master") and
                not file_ops.get_target_file("~/.bak_pasty", "master", "master")
            ):
                missing_master = True
            
            if (
                not file_ops.get_target_file("~/.pasty", "salt", "salt") and
                not file_ops.get_target_file("~/.bak_pasty", "salt", "salt")
            ):
                missing_salt = True
            
            if not file_ops.get_target_file("~/.pasty", "master", "master"):
                file_ops.make_copies("~/.bak_pasty/master/master", "~/.pasty/master/master")
                missing_master = False
            
            if not file_ops.get_target_file("~/.bak_pasty", "master", "master"):
                file_ops.make_copies("~/.pasty/master/master", "~/.bak_pasty/master/master")
                missing_master = False
            
            if not file_ops.get_target_file("~/.pasty", "salt", "salt"):
                file_ops.make_copies("~/.bak_pasty/salt/salt", "~/.pasty/salt/salt")
                missing_salt = False

            if not file_ops.get_target_file("~/.bak_pasty", "salt", "salt"):
                file_ops.make_copies("~/.pasty/salt/salt", "~/.pasty/salt/salt")
                missing_salt = False

            if missing_master:
                return 0
            if missing_salt:
                return 1
            else:
                return 2

    def check_public_key_exists(self):
        missing_gpg_key = False
        if file_ops.get_target_file("~/.pasty", "gpg", "public_key.asc") and file_ops.get_target_file("~/.bak_pasty", "gpg", "public_key.asc"):
            return True
        else:
            if (
                not file_ops.get_target_file("~/.pasty", "gpg", "public_key.asc") and
                not file_ops.get_target_file("~/.bak_pasty", "gpg", "public_key.asc")
               ):
                missing_gpg_key = True
            
            if not file_ops.get_target_file("~/.bak_pasty", "gpg", "public_key.asc"):
                file_ops.make_copies("~/.pasty/gpg/public_key.asc", "~/.pasty/gpg/public_key.asc")
                return True

            if not file_ops.get_target_file("~/.pasty", "gpg", "public_key.asc"):
                file_ops.make_copies("~/.bak_pasty/gpg/public_key.asc", "~/.pasty/gpg/public_key.asc")
                return True

            if missing_gpg_key:
                return None

    def get_name(self):
        while True:
            name        = input("What's your name?\n")
            check_name  = input("Please type your name again.\n")
            if name == check_name:
                return name
            elif name != check_name:
                print("Name don't match!")

    def get_email(self):
        while True:
            email       = input("What's your email?\n")
            check_email = input("Please type your email again\n")  
            if email == check_email:
                return email
            elif email!= check_email:
                print("Emails don't match!")

    def check_for_pasty_files(self):
        upper_dirs      = [os.path.expanduser("~/.pasty"), os.path.expanduser("~/.bak_pasty")]
        directories     = ["master", "salt", "gpg", "logs"]
        files_to_check  = {
            "master": ["master"],
            "salt"  : ["salt"],
            "gpg"   : ["public_key.asc"],
            "logs"  : ["master", "keys", "salt", "gpg"]
                          }
        for upper_dir in upper_dirs:
            for directory in directories:
                path = os.path.join(upper_dir, directory)
                if os.path.exists(path):
                    for file in files_to_check[directory]:
                        if not os.path.exists(os.path.join(path, file)):
                            return None 
                else:
                    return None
        return True

