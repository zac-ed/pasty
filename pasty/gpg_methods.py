import subprocess
import os
from pasty_logging      import Logging
from file_operations    import FileOperations

file_ops = FileOperations()
logging  = Logging()

class GpgMethods:

    def __init__(self):
        self.key_files = [
                os.path.expanduser("~/.pasty/gpg/public_key.asc"), 
                os.path.expanduser("~/.bak_pasty/gpg/public_key.asc")
                         ]

    def gpg_gen_key(self, name, email):
        """
        Generates a GPG key pair for ECC signed and encrpyted option 
        """
        key_generation_script = f"""
        %echo Generating a default key
        %no-protection
        %transient-key
        Key-Type: 9
        Key-Length: 3072
        Name-Real: {name}
        Name-Email: {email} 
        Expire-Date: 6m
        %commit
        %echo done
        """

        try:
            result = subprocess.run(
                ['gpg', '--batch', '--gen-key'],
                input          = key_generation_script,
                text           = True,
                check          = True,
                capture_output = True
                                   )

            output = result.stdout
            return output
        except subprocess.CalledProcessError as e:
            error = f"Error running GnuPG: {e}"
            print(error)
            logging.log_message(message=error, log_type="gpg_log")
            return None

    def encrypt_data(self, input_file, output_file, recipient):
        '''
        Encrypts data using the recipient's public key.
        '''
        ### encrypt_data takes an input_file (the file you want 
        ### to encrypt), an output_file (where the encrypted 
        ### data will be saved), and the recipient (the name or
        ### email associated with the recipient's public key).
        try:
            subprocess.run(
                    ["gpg", "--encrypt", "--recipient", recipient, "--output", output_file, input_file], 
                    check=True
                          )
            return True
        except subprocess.CalledProcessError as e:
            error = f"Error encrypting data: {e}"
            print(error)
            logging.log_message(message=error, log_type="gpg")
            return False

    def decrypt_data(self, input_file):
        '''
        validates
        '''
        try:
            subprocess.run(
                    ["gpg", "--decrypt", input_file], 
                    check=True
                          )
            return True
        except subprocess.CalledProcessError as e:
            error = f"Error decrypting file: {e}"
            print(error)
            logging.log_message(message=error, log_type="gpg")
            return False

    def export_public_key(self, user_name):
        '''
        writes the public key to an output_file.
        This will be ~/.pasty/gpg/public_key.asc
        '''
        try:
            # Run the GPG command to export the public key
            for key_file in self.key_files:
                subprocess.run(
                    ["gpg", "--export", "-a", user_name], 
                    stdout=open(key_file, "w"), 
                    check=True
                              )
                logging.log_message(message="public key exported", log_type="gpg")
                file_ops.set_file_permissions(key_file)
            return True
        except subprocess.CalledProcessError as e:
            error = f"Error exporting public key: {e}"
            print(error)
            logging.log_message(message=error, log_type="gpg")
            return False
