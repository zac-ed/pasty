import sys
from master_passwd      import MasterPasswd
from service_passwd     import ServicePasswd
from file_operations    import FileOperations
from gpg_methods        import GpgMethods
from checks             import Checks
from pasty_logging      import Logging
from pasty_ops          import PastyOperations

gpg_methods     = GpgMethods()
service_pass    = ServicePasswd()
master_pass     = MasterPasswd()
pasty_ops       = PastyOperations()
file_ops        = FileOperations()
checks          = Checks()
logging         = Logging()

def main():
    if checks.check_first_time():
        if not checks.check_for_pasty_files():
            files = [
                    file_ops.full_path("~/.pasty", "logs", "initialised"), 
                    file_ops.full_path("~/.bak_pasty", "logs", "initialised")
                    ]
            file_ops.write_to_file(
                    files[0], 
                    "Placeholder file for pasty initialisation check.", 
                    "logs", 
                    "master", 
                    "master"
                                  )
            file_ops.make_copies(files[0], files[1])

            for file in files:
                file_ops.set_file_permissions(file)
            
            user_name = checks.get_name()
            email     = checks.get_email()
            gpg_methods.gpg_gen_key(user_name, email)
            gpg_methods.export_public_key(user_name)
            
            pasty_ops.masterpw_creation()
            
        elif checks.check_for_pasty_files():
            print("Error running pasty, deleted initialised file")
            
            
    result          = checks.check_master_exists()
    error_messages  = {
        0: "error: missing master key",
        1: "error: missing salt key",
        2: "error: missing both master and salt keys"
                      }

    if result in error_messages:
        print(error_messages[result])
    
    if checks.check_master_exists() and checks.check_public_key_exists():

        entered_password = master_pass.enter_password()
        salts            = [file_ops.get_target_file()]
        salt             = file_ops.retrieve_file_content()
#        master_pass.verify_master_password(entered_password, hashed_password, salt)
        if sys.argv[2] == "get" and sys.argv[3]:
            pasty_ops.get_service_pw(sys.argv[3])
        elif sys.argv[2] == "new" and sys.argv[3]:
            pasty_ops.new_service_pw(sys.argv[3])
        elif sys.argv[2] == "passwd":
            if sys.argv[3]:
                if file_ops.get_target_file("~/.pasty", "keys", sys.argv[3]):
                    pasty_ops.new_service_pw(sys.argv[3])
                elif file_ops.get_target_file("~/.bak_pasty", "keys", sys.argv[3]):
                    pasty_ops.new_service_pw(sys.argv[3])
                else:
                    print("the service does not exist.")
            if not sys.argv[3]:
                pasty_ops.masterpw_creation()
            else:
                print("Unknown error")
                logging.log_message("master", message="Something weird happened.")

        elif sys.argv[2] == None:
            pass

if __name__ == '__main__':
    if checks.check_sudo():
        print("do not run pasty with sudo.")
        pasty_ops.print_help()
        exit(1)
    if len(sys.argv) >= 4:
        print("pasty won't work if you do that!")
        pasty_ops.print_help()
        exit(1)
    main()

#        if  and sys.argv[1] != "help":
#
#            user_passwd = master_pass.enter_password()
#            master_pw   = master_pass.get_master_pw()
#            salt        = master_pass.get_salt()
#            
#            verify = master_pass.verify_master_password(user_passwd, master_pw, salt)
#            
#            if verify == True:
#                main()
#            else:
#                print("Try pasty help")
#
#        elif sys.argv[1] == "help":
#            print("Usage:\n\npasty get service --fetch a password saved for a service named service\n\npasty new service --create and store a new password for a service named service\n\npasty passwd --change master passwd")
#    else:

    # if file_ops.get_target_file("~/.pasty/", "master", "master") and file_ops.get_target_file("~/.pasty/", "salt", "salt"):
    #     pass
    # elif file_ops.get_target_file("~/.bak_pasty/", "master", "master") and file_ops.get_target_file("~/.bak_pasty/", "salt", "salt"):
    #     pass
    # else:
    #     file_ops.make_copies()

#        master_files     = [
#                file_ops.retrieve_file_content(
#                    file_ops.full_path("~/.pasty", "master", "master"), "master"
#                                              ), 
#                file_ops.retrieve_file_content(
#                    file_ops.full_path("~/.bak_pasty", "master", "master"), "master"
#                                              )
#                           ]
#        salt_files       = [
#                file_ops.retrieve_file_content(
#                    file_ops.full_path("~/.pasty", "salt", "salt"), "master"
#                                              ), 
#                file_ops.retrieve_file_content(
#                    file_ops.full_path("~/.bak_pasty", "salt", "salt"), "master"
#                                              )
#                           ]

#        file_checks      = [
#                file_ops.get_target_file("~/.pasty", "master", "master"), 
#                file_ops.get_target_file("~/.bak_pasty", "master", "master"),
#                file_ops.get_target_file("~/.pasty", "salt", "salt"), 
#                file_ops.get_target_file("~/.bak_pasty", "salt", "salt")
#                           ]
#        paths            = [
#                file_ops.full_path("~/.pasty", "master", "master"),
#                file_ops.full_path("~/.bak_pasty", "master", "master"),
#                file_ops.full_path("~/.pasty", "salt", "salt"),
#                file_ops.full_path("~/.bak_pasty", "salt", "salt")
#                           ]
#
#        if not file_checks[0]:
#            file_ops.make_copies(
#                    file_checks[1], 
#                    paths[0]
#                                )
#            file_ops.set_file_permissions(paths[0])
#
#        if not file_checks[1]:
#            file_ops.make_copies(
#                    file_checks[0], 
#                    paths[1]
#                                )
#            file_ops.set_file_permissions(paths[1])    
#
#        if not file_checks[2]:
#            file_ops.make_copies(
#                    file_checks[3], 
#                    paths[2]
#                                )
#            file_ops.set_file_permissions(paths[2])
#        if not file_checks[3]:
#            file_ops.make_copies(
#                    file_checks[2], 
#                    paths[3]
#                                )
#            file_ops.set_file_permissions(paths[3])
