import bcrypt
import os
from   getpass          import getpass
from   file_operations  import FileOperations
from   pasty_logging    import Logging

file_ops = FileOperations()
logging  = Logging()

class MasterPasswd:
    
    def __init__(self):
        self.directory      = os.path.expanduser("~/.pasty/master")
        self.bak_directory  = os.path.expanduser("~/.bak_pasty/master")

        self.salt_dir       = os.path.expanduser("~/.pasty/salt")
        self.salt_bakdir    = os.path.expanduser("~/.bak_pasty/salt")

        self.master_files   = [
                os.path.join(self.directory, "master"),
                os.path.join(self.bak_directory, "master"),
                              ]
        self.salt_files     = [
                os.path.join(self.salt_dir, "salt"),
                os.path.join(self.salt_bakdir, "salt")
                              ]

    def make_new_master_password(self, salt):
        '''
        Prompts for new password and writes the password to a file
        '''
        master_password         = getpass(prompt="Enter your master password: ")
        hashed_master_password  = bcrypt.hashpw(master_password.encode("utf-8"), salt)

        for file in self.master_files:
            file_ops.write_to_file(
                    file, 
                    hashed_master_password, 
                    "master", 
                    "master", 
                    "master"
                                  )
        logging.log_message(
                message="changed master password",
                log_type="master" 
                           )

    def make_master_salt(self):
        '''
        Writes a generated salt to a file
        '''
        salt        = bcrypt.gensalt()
        for file in self.salt_files:
            file_ops.write_to_file(
                    file, 
                    salt, 
                    "master", 
                    "master", 
                    "master"
                                  )
            logging.log_message(
                    message="new salt generated",
                    log_type="master" 
                               )
            file_ops.set_file_permissions(file)

    def enter_password(self):
        '''
        Returns the hashed password that the user writes 
        '''
        entered_password = getpass(prompt="Enter your master password: ")
        salt             = bcrypt.gensalt()
        entered_password = bcrypt.hashpw(entered_password.encode("utf-8"), salt)
        return entered_password

    def verify_master_password(self, entered_password, hashed_password, salt):
        '''
        Returns true if the entered password hash 
        and master password hash match
        '''
        entered_password_hash = bcrypt.hashpw(entered_password.encode("utf-8"), salt)
        if entered_password_hash == hashed_password:
            logging.log_message(
                    message="master password verified successfully",
                    log_type="master_log" 
                               )
            return True
        else:
            return False
