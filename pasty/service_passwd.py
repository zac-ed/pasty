import os
from cryptography.fernet import Fernet
from pasty_logging       import Logging 

logging = Logging()

class ServicePasswd:

    def create_key(self):
        '''
        Returns a symmetrical encryption key
        '''
        key          = Fernet.generate_key()
        cipher_suite = Fernet(key)
        logging.log_message(log_type="keys_log", key_type="Fernet")
        return cipher_suite

    def make_passwd(self):
        '''
        Returns a random string of 25 chars
        '''
        data = os.urandom(25)
        return data

    def encrypt_data(self, cipher_suite, data):
        '''
        Returns encrypted data based on 
        outputs of create_key and make_passwd
        '''
        encrypted_data  = cipher_suite.encrypt(data)
        return encrypted_data

    def decrypt_data(self, cipher_suite, encrypted_data):
        '''
        Decrypts and returns data 
        '''
        decrypted_data = cipher_suite.decrypt(encrypted_data)
        return decrypted_data
