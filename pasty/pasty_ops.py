from master_passwd      import MasterPasswd
from file_operations    import FileOperations
from pasty_logging      import Logging

file_ops    = FileOperations()
master_pass = MasterPasswd()
logging     = Logging()

class PastyOperations:

    def get_service_pw(self, service):
        ###    DONT KEEP IT LIKE THIS   ###
        ### IT SHOULDN"T RETURN SERVICE ###
        return service 

    def new_service_pw(self, service):
        ###    DONT KEEP IT LIKE THIS   ###
        ### IT SHOULDN"T RETURN SERVICE ###
        return service

    def masterpw_creation(self):
        master_pass.make_master_salt()
        if file_ops.get_target_file("~/.pasty", "salt", "salt"):
            salt = file_ops.retrieve_file_content(
                    file_ops.full_path("~/.pasty", "salt", "salt"), "salt"
                                                 )
            master_pass.make_new_master_password(salt)
        if not file_ops.get_target_file("~/.pasty", "salt", "salt") and file_ops.get_target_file("~/.bak_pasty", "salt", "salt"):
            salt = file_ops.retrieve_file_content(
                    file_ops.full_path("~/.bak_pasty", "salt", "salt"), "salt"
                                                 )
            file_ops.make_copies(
                    file_ops.full_path("~/.bak_pasty", "salt", "salt"), 
                    file_ops.full_path("~/.pasty", "salt", "salt")
                                )
            master_pass.make_new_master_password(salt)
        else:
            print(
                    "Something went wrong, salts were deleted in the master password creation process"
                 )
            logging.log_message(
                    log_type="master", 
                    message="salts deleted during master creation, likely a file system error"
                               )

    def print_help(self):
        print("Usage:\n\npasty get foo --fetch a password saved for a service named foo\n\npasty new foo --create and store a new password for a service named foo\n\npasty passwd --change master passwd\n\npasty passwd foo --change password for a service named foo")
