# Pasty

## Description
This unfinished project aims to be a secure command-line password manager for linux, utilising symmetric encryption via Fernet for password keys, a hashed master password for validation, and relying on GPG to encrypt the password keys themselves.

## Installation
Via ./install.sh on the command line. This will install some pip dependencies, move files, set permissions, and put a script wrapper on your shell path so the program can be called via just its name.

## Usage
pasty get foo --fetch a password saved for a service named foo
pasty new foo --create and store a new password for a service named foo
pasty passwd --change master passwd
pasty passwd foo --change password for a service named foo"

## Contributing
The program requires pip, bcrypt, cryptography.Fernet, getpass, os, sys, datetime, and subprocess, in python and the install script and wrapper are for bash. I'm open to any contributions!

## License
GPL3.0