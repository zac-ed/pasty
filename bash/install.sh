#!/bin/bash

	# install dependencies #
mkdir -p ~/.local/venv
python3 -m venv ~/.local/venv/modules
source ~/.local/venv/modules/bin/activate
pip install bcrypt cryptography

	#  To the end user,  don't be an  #
	#  idiot and deactivate the venv  #
	#      as pasty will break  	  #
	###################################
	# I assume getpass is in std libs #
	#  go install getpass separately  #
	# 	 if that's a nah!	  #

dirs=("gpg" "keys" "logs" "master" "services" "salt")

python_output=$(python3 -c "import time; print(time.time())")

lock="False"
lock+="
$python_output"

username="$(whoami)"

for subdir in "${dirs[@]}"
do
    mkdir -p ~/.pasty/$subdir ~/.bak_pasty/$subdir

    chown -R $username:$username ~/.pasty/$subdir
    chown -R $username:$username ~/.bak_pasty/$subdir
    
    chmod 700 ~/.pasty/$subdir ~/.bak_pasty/$subdir
done

touch ~/.pasty/logs/lock

printf "%s\n%s" "False" "$python_output" > ~/.pasty/logs/lock


cp -r ../pasty_files ~/.local/bin/
cp pasty ~/.local/bin

chmod +x ~/.local/bin/pasty_files/*py ~/.local/bin/pasty 
